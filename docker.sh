#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export PROJECT_PATH=GIT/hidden-tits-in-the-park
export CASTLE_ENGINE_PATH=/home/castle_game_engine

export FPCLAZARUS_VERSION=3.3.1
export PATH=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/bin/:${PATH}
FPCLAZARUS_REAL_VERSION=`fpc -iV`
echo 'Real FPC version:' ${FPCLAZARUS_REAL_VERSION}
export FPCDIR=/usr/local/fpclazarus/${FPCLAZARUS_VERSION}/fpc/lib/fpc/${FPCLAZARUS_REAL_VERSION}/

export HOME=/home/
echo 'Building CGE build tool:'
cd "$CASTLE_ENGINE_PATH"
rm -f ./tools/build-tool/castle-engine
./tools/build-tool/castle-engine_compile.sh
cp tools/build-tool/castle-engine /usr/local/bin/
castle-engine --version

echo 'Building Android:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --target=android --mode=release --package-format=android-apk --verbose >/home/${PROJECT_PATH}/build_android.log
echo 'Building Debian64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=linux --cpu=x86_64 --mode=release --package-format=deb --verbose >/home/${PROJECT_PATH}/build_deb.log
echo 'Building Linux64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=linux --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_linux.log
#echo 'Building Win32:'
#castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=win32 --cpu=i386 --mode=release --verbose >/home/${PROJECT_PATH}/build_win32.log
echo 'Building Win64:'
castle-engine --project=/home/${PROJECT_PATH}/ --output=/home/${PROJECT_PATH}/ package --os=win64 --cpu=x86_64 --mode=release --verbose >/home/${PROJECT_PATH}/build_win64.log

echo 'Done.'

# After all builds and operations on the container finished:
# docker container stop cge
