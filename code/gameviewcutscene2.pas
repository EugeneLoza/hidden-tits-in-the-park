{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameViewCutscene2;

interface

uses Classes,
  CastleVectors, CastleUIControls, CastleControls;

type
  TViewCutscene2 = class(TCastleView)
  strict private
    TimeElapsed: Single;
    procedure ClickAgain(Sender: TObject);
    procedure ClickMenu(Sender: TObject);
  published
    DialogueBox1, DialogueBox2, DialogueBox3, DialogueBox4, DialogueBox5, DialogueBox6, DialogueBox7: TCastleUserInterface;
    DialogueText1, DialogueText2, DialogueText3, DialogueText4, DialogueText5, DialogueText6, DialogueText7: TCastleLabel;
    ButtonMenu, ButtonAgain: TCastleButton;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: boolean); override;
  end;

var
  ViewCutscene2: TViewCutscene2;

implementation
uses
  SysUtils,
  GameViewMain, GameViewMainMenu;

procedure TViewCutscene2.ClickAgain(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.View := ViewMain;
end;

procedure TViewCutscene2.ClickMenu(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.View := ViewMainMenu;
end;

constructor TViewCutscene2.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewcutscene2.castle-user-interface';
end;

procedure TViewCutscene2.Start;
begin
  inherited;
  TimeElapsed := 0;

  ButtonAgain.OnClick := @ClickAgain;
  ButtonMenu.OnClick := @ClickMenu;

  DialogueBox1.Exists := false;
  DialogueBox2.Exists := false;
  DialogueBox3.Exists := false;
  DialogueBox4.Exists := false;
  DialogueBox5.Exists := false;
  DialogueBox6.Exists := false;
  DialogueBox7.Exists := false;
end;

procedure TViewCutscene2.Update(const SecondsPassed: Single; var HandleInput: boolean);
const
  TypingSpeed = 40;
  TypingPause = 20;
var
  DisplayCharacters: Integer;
begin
  inherited;

  TimeElapsed += SecondsPassed;
  DisplayCharacters := Round(TimeElapsed * TypingSpeed);

  DialogueText1.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText1.Caption.Length + TypingPause;
  DialogueText2.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText2.Caption.Length + TypingPause;
  DialogueText3.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText3.Caption.Length + TypingPause;
  DialogueText4.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText4.Caption.Length + TypingPause;
  DialogueText5.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText5.Caption.Length + TypingPause;
  DialogueText6.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText6.Caption.Length + TypingPause;
  DialogueText7.MaxDisplayChars := DisplayCharacters;

  DialogueBox1.Exists := DialogueText1.MaxDisplayChars > 0;
  DialogueBox2.Exists := DialogueText2.MaxDisplayChars > 0;
  DialogueBox3.Exists := DialogueText3.MaxDisplayChars > 0;
  DialogueBox4.Exists := DialogueText4.MaxDisplayChars > 0;
  DialogueBox5.Exists := DialogueText5.MaxDisplayChars > 0;
  DialogueBox6.Exists := DialogueText6.MaxDisplayChars > 0;
  DialogueBox7.Exists := DialogueText7.MaxDisplayChars > 0;
end;

end.
