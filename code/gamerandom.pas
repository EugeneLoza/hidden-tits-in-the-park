unit GameRandom;

{$mode ObjFPC}{$H+}

interface

uses
  CastleRandom;

var
  Rnd: TCastleRandom;

implementation
uses
  SysUtils;

initialization
  Rnd := TCastleRandom.Create;
finalization
  FreeAndNil(Rnd);
end.

