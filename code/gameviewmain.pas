{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameViewMain;

interface

uses
  Classes, Generics.Collections, Generics.Defaults,
  CastleFindFiles,
  CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse, CastleShiftedImage, StarFallUnit;

type
  TCastleShiftedImageHelper =  class helper for TCastleShiftedImage
  public
    function PixelPerfectClick(const ClickX, ClickY: Integer): Boolean;
  end;

type
  TLayer = class
  public
    IsFound: Boolean;
    IsVariant: Boolean;
    Image: TCastleShiftedImage;
    Variant: TCastleShiftedImage;
    destructor Destroy; override;
  end;
  TLayersDictionary = specialize TObjectDictionary<string, TLayer>;
  TLayersList = specialize TObjectList<TLayer>;

type
  TViewMain = class(TCastleView)
  strict private
    TimeElapsed: Single;
    GameWon: Boolean;
    Layers: TLayersDictionary;
    Files: TStringList;
    SortedLayerNames: TStringList;
    ActiveLayers: TLayersList;
    LastLoadedFile: Integer;
    procedure ClickMenu(Sender: TObject);
    procedure ClickWin(Sender: TObject);
    procedure ImageRelease(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
    procedure RandomizeImage;
    procedure BuildImage;
    procedure FoundFile(const FileInfo: TFileInfo; var StopSearch: Boolean);
    procedure WinGame;
    function TimeToString(const ASeconds: Single): String;
    function HighScore: Single;
  public
    IsLoadingNeeded: Boolean;
    procedure StartLoadLayers;
    procedure FinishLoadLayers;
    function LoadNextFile: Single;
    function LoadingNeeded: Boolean;
  strict private
    MapImage: TCastleImageControl;
    ScrollViewGameplayArea: TCastleScrollView;
    LabelGirlsRemaining: TCastleLabel;
    LabelCurrentTime: TCastleLabel;
    LabelHighScore: TCastleUserInterface;
    LastNya: String;
    ClickedLayer: Integer;
    LastTrack: String;
    StarFall: TStarFallEffect;
    procedure ImagePress(const Sender: TCastleUserInterface;
      const Event: TInputPressRelease; var Handled: Boolean);
  public
    class procedure PopSound;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    procedure GLContextOpen; override;
    procedure Resize; override;
  end;

var
  ViewMain: TViewMain;

implementation

uses
  SysUtils,
  CastleLog, CastleSoundEngine, CastleGlImages, CastleImages, CastleRectangles,
  CastleColors, CastleVectors, CastleGLShaders, CastleConfig,
  GameViewMainMenu, GameViewCutscene2, GameRandom, GameScreenEffect;

function TCastleShiftedImageHelper.PixelPerfectClick(const ClickX,
  ClickY: Integer): Boolean;
begin
  Exit(
    (ClickX >= ShiftX) and (ClickX <= ShiftX + Image.Width) and
    (ClickY >= ShiftY) and (ClickY <= ShiftY + Image.Height) and
    (TRGBAlphaImage(Image).PixelPtr(ClickX - ShiftX, ClickY - ShiftY)^.W > 0)
  );
end;

destructor TLayer.Destroy;
begin
  FreeAndNil(Image);
  FreeAndNil(Variant);
  inherited Destroy;
end;

{ TViewMain ----------------------------------------------------------------- }

procedure TViewMain.StartLoadLayers;
begin
  Files := TStringList.Create;
  FindFiles('castle-data:/map/8k/', '*.png', false, @FoundFile, []);
  Files.Sort;

  Layers.Clear;
  LastLoadedFile := -1;
end;

procedure TViewMain.FinishLoadLayers;
var
  LayerName: String;
  VariantLayersCount: Integer;
begin
  SortedLayerNames.Clear;
  VariantLayersCount := 0;
  for LayerName in Layers.Keys do
  begin
    SortedLayerNames.Add(LayerName);
    if Layers[LayerName].Variant = nil then
      WriteLnLog('Layer %s has no variant', [LayerName]);
    if Layers[LayerName].Image = nil then
      raise Exception.CreateFmt('Layer %s has no image', [LayerName]);
    if Layers[LayerName].Variant <> nil then
      Inc(VariantLayersCount);
  end;
  WriteLnLog('Total layers %d', [Layers.Count]);
  WriteLnLog('Total variant layers %d', [VariantLayersCount]);
  SortedLayerNames.Sort;

  IsLoadingNeeded := false;

  FreeAndNil(Files);
end;

function TViewMain.LoadNextFile: Single;
const
  VariantSignature = '_VARIANT.png';
  ExtensionSignature = '.png';
var
  CurrentLayer: TLayer;
  LayerName: String;
begin
  Inc(LastLoadedFile);
  if LastLoadedFile >= Files.Count then
    Exit(1.0);
  Result := Single(LastLoadedFile) / Single(Files.Count);
  if Copy(Files[LastLoadedFile], Length(Files[LastLoadedFile]) - Length(VariantSignature) + 1, Length(VariantSignature)) = VariantSignature then
  begin
    LayerName := Copy(Files[LastLoadedFile], 1, Length(Files[LastLoadedFile]) - Length(VariantSignature));
    if not Layers.TryGetValue(LayerName, CurrentLayer) then
    begin
      CurrentLayer := TLayer.Create;
      Layers.Add(LayerName, CurrentLayer);
    end else
      if CurrentLayer.Variant <> nil then
        raise Exception.Create('CurrentLayer.Variant <> nil at ' + Files[LastLoadedFile]);
    CurrentLayer.Variant := TCastleShiftedImage.Create;
    CurrentLayer.Variant.Url := 'castle-data:/map/8k/' + Files[LastLoadedFile];
  end else
  begin
    LayerName := Copy(Files[LastLoadedFile], 1, Length(Files[LastLoadedFile]) - Length(ExtensionSignature));
    if not Layers.TryGetValue(LayerName, CurrentLayer) then
    begin
      CurrentLayer := TLayer.Create;
      Layers.Add(LayerName, CurrentLayer);
    end else
      if CurrentLayer.Image <> nil then
        raise Exception.Create('CurrentLayer.Image <> nil at ' + Files[LastLoadedFile]);
    CurrentLayer.Image := TCastleShiftedImage.Create;
    CurrentLayer.Image.Url := 'castle-data:/map/8k/' + Files[LastLoadedFile];
  end;
end;

function TViewMain.LoadingNeeded: Boolean;
begin
  Exit(IsLoadingNeeded);
end;

procedure TViewMain.RandomizeImage;
var
  I: Integer;
  Count: SizeInt;
  CurrentLayer: TLayer;
begin
  ActiveLayers.Clear;
  for CurrentLayer in Layers.Values do
  begin
    CurrentLayer.IsVariant := false;
    CurrentLayer.IsFound := false;;
  end;
  Count := 0;
  repeat
    I := Rnd.Random(SortedLayerNames.Count);
    CurrentLayer := Layers[SortedLayerNames[I]];
    if (CurrentLayer.Variant <> nil) and (not CurrentLayer.IsVariant) then
    begin
      CurrentLayer.IsVariant := true;
      ActiveLayers.Add(CurrentLayer);
      Inc(Count);
    end;
  until Count >= 21;
end;

procedure TViewMain.BuildImage;
var
  Image: TDrawableImage;
  //BackgroundHack: TDrawableImage;
  //BlankImage: TRGBAlphaImage;
  I: Integer;
  CurrentLayer: TLayer;
  DrawRect: TFloatRectangle;

  procedure AttachShader;
  var
    VS, FS: String;
    BurnShader: TGLSLProgram;
  begin
    VS := 'attribute vec2 vertex;' + LineEnding +
          'attribute vec2 tex_coord;' + LineEnding +
          'uniform vec2 viewport_size;' + LineEnding +
          'varying vec2 tex_coord_frag;' + LineEnding +
          'void main(void)' + LineEnding +
          '{' + LineEnding +
          '  gl_Position = vec4(vertex * 2.0 / viewport_size - vec2(1.0), 0.0, 1.0);' + LineEnding +
          '  tex_coord_frag = tex_coord;' + LineEnding +
          '}' + LineEnding;
    FS := 'varying vec2 tex_coord_frag;' + LineEnding +
          'uniform sampler2D image_texture;' + LineEnding +
          'void main(void)' + LineEnding +
          '{' + LineEnding +
          'gl_FragColor = texture2D(image_texture, tex_coord_frag);' + LineEnding +
          'gl_FragColor = vec4(gl_FragColor.r,gl_FragColor.g,gl_FragColor.b,1.0 - gl_FragColor.r);' +
          '}' + LineEnding;

    //FreeAndNil(BurnShader);
    BurnShader := TGLSLProgram.Create;
    BurnShader.Name := 'TDistanceFieldCut';
    BurnShader.AttachVertexShader(VS);
    BurnShader.AttachFragmentShader(FS);
    BurnShader.Link;
    Image.CustomShader := BurnShader;
  end;

begin
  Image := TDrawableImage.Create(Layers['0000_BACKGROUND'].Image.Image, true, false);
  Image.RenderToImageBegin;
  DrawRect := FloatRectangle(0, 0, Image.Width, Image.Height);
  for I := 0 to Pred(SortedLayerNames.Count) do
  begin
    CurrentLayer := Layers[SortedLayerNames[I]];

    if CurrentLayer.IsVariant then
    begin
      if CurrentLayer.IsFound then
        CurrentLayer.Variant.Color := Vector4(0.8, 1.0, 0.8, 1.0)
      else
        CurrentLayer.Variant.Color := White;
      CurrentLayer.Variant.Draw(DrawRect)
    end else
    begin
      //CurrentLayer.Image.Color := White; --- it's always white
      CurrentLayer.Image.Draw(DrawRect);
    end;
  end;
  Image.RenderToImageEnd;

  {AttachShader;
  BlankImage := TRGBAlphaImage.Create(Image.Width, Image.Height);
  BlankImage.Clear(Vector4Byte(0,0,0,0));
  BackgroundHack := TDrawableImage.Create(BlankImage,true,true);
  BackgroundHack.Alpha := acBlending;
  BackgroundHack.RenderToImageBegin;
  Image.Draw(0, 0);
  BackgroundHack.RenderToImageEnd;

  MapImage.DrawableImage := BackgroundHack;}
  MapImage.DrawableImage := Image;
  MapImage.OwnsDrawableImage := true;
  LabelGirlsRemaining.Caption := ActiveLayers.Count.ToString;
end;

procedure TViewMain.FoundFile(const FileInfo: TFileInfo; var StopSearch: Boolean);
begin
  Files.Add(FileInfo.Name);
end;

procedure TViewMain.WinGame;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('cheering', true));
  (DesignedComponent('ContainerGirlsRemaining') as TCastleUserInterface).Exists := false;
  (DesignedComponent('ButtonMenu') as TCastleUserInterface).Exists := false;
  (DesignedComponent('ButtonWin') as TCastleUserInterface).Exists := true;
  StarFall.Start;
  GameWon := true;
  if TimeElapsed < HighScore then
  begin
    UserConfig.SetFloat('high_score', TimeElapsed);
    UserConfig.Save;
    LabelHighScore.Exists := true;
  end;
end;

function TViewMain.TimeToString(const ASeconds: Single): String;
var
  M, S: Integer;
  Seconds: String;
begin
  M := Trunc(ASeconds / 60);
  S := Round(ASeconds - M * 60);
  Seconds := S.ToString;
  if S < 10 then
    Seconds := '0' + Seconds;
  Exit(M.ToString + ':' + Seconds);
end;

function TViewMain.HighScore: Single;
begin
  Exit(UserConfig.GetFloat('high_score', 60 * 99 + 59));
end;

procedure TViewMain.ImagePress(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
var
  ClickX, ClickY: Integer;

  procedure DrawDebugClick;
  var
    Debugi: TRGBAlphaImage;
    Debug: TDrawableImage;
  begin
    MapImage.DrawableImage.RenderToImageBegin;
    Debugi := TRGBAlphaImage.Create(3, 3);
    Debugi.Clear(Black);
    Debugi.PixelPtr(1, 1)^ := Vector4Byte(255,255,255,255);
    Debug := TDrawableImage.Create(Debugi, true, true);
    Debug.Draw(ClickX, ClickY);
    WriteLnLog('height = %n; translation = %n; scale = %n', [ScrollViewGameplayArea.ScrollArea.EffectiveHeight, ScrollViewGameplayArea.ScrollArea.Translation.Y, UiScale]);
    Debug.Free;
    MapImage.DrawableImage.RenderToImageEnd;
  end;

var
  I: Integer;
begin
  if not (Event.EventType = itMouseButton) then
    Exit;

  ClickedLayer := -1;
  ClickX := Round(Event.Position.X / Container.PixelsWidth * MapImage.DrawableImage.Width);
  ClickY := Round((Event.Position.Y + (ScrollViewGameplayArea.ScrollArea.EffectiveHeight - ScrollViewGameplayArea.ScrollArea.Translation.Y - Container.UnscaledHeight) * UiScale) / Container.PixelsWidth * MapImage.DrawableImage.Width);
  for I := 0 to Pred(ActiveLayers.Count) do
    if ActiveLayers[I].Variant.PixelPerfectClick(ClickX, ClickY) then
    begin
      ClickedLayer := I;
      //WriteLnLog('Clicked Layer %d', [ClickedLayer]);
      ImageRelease(MapImage, Event, Handled); {$WARNING Workaround that MapImage doesn't receive OnRelease event}
      Exit;
    end;
end;

procedure TViewMain.ImageRelease(const Sender: TCastleUserInterface;
  const Event: TInputPressRelease; var Handled: Boolean);
var
  ClickX, ClickY: Integer;
  NewNya: String;
begin
  if not (Event.EventType = itMouseButton) then
    Exit;

  ClickX := Round(Event.Position.X / Container.PixelsWidth * MapImage.DrawableImage.Width);
  ClickY := Round((Event.Position.Y + (ScrollViewGameplayArea.ScrollArea.EffectiveHeight - ScrollViewGameplayArea.ScrollArea.Translation.Y - Container.UnscaledHeight) * UiScale) / Container.PixelsWidth * MapImage.DrawableImage.Width);

  if (ClickedLayer >= 0) and ActiveLayers[ClickedLayer].Variant.PixelPerfectClick(ClickX, ClickY) then
  begin
    //WriteLnLog('Unclicked Layer %d', [ClickedLayer]);
    ActiveLayers[ClickedLayer].IsFound := true;
    PopSound;
    repeat
      NewNya := 'nya' + (1 + Rnd.Random(14)).ToString;
    until NewNya <> LastNya;
    SoundEngine.Play(SoundEngine.SoundFromName(NewNya, true));
    LastNya := NewNya;
    ActiveLayers.Delete(ClickedLayer);
    BuildImage;
    if ActiveLayers.Count = 0 then
      WinGame;
  end;
  ClickedLayer := -1;
end;

procedure TViewMain.ClickMenu(Sender: TObject);
begin
  PopSound;
  Container.View := ViewMainMenu;
end;

procedure TViewMain.ClickWin(Sender: TObject);
begin
  PopSound;
  Container.View := ViewCutscene2;
end;

class procedure TViewMain.PopSound;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('pop' + (1 + Rnd.Random(9)).ToString, true));
end;

constructor TViewMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewmain.castle-user-interface';
  DesignPreload := true;
  IsLoadingNeeded := true;
  Layers := TLayersDictionary.Create([doOwnsValues]);
  SortedLayerNames := TStringList.Create;
  ActiveLayers := TLayersList.Create(false);
  LastTrack := '';
end;

destructor TViewMain.Destroy;
begin
  FreeAndNil(SortedLayerNames);
  FreeAndNil(ActiveLayers);
  FreeAndNil(Layers);
  inherited Destroy;
end;

procedure TViewMain.Start;
var
  NewTrack: String;
begin
  inherited;
  TimeElapsed := 0;
  GameWon := false;

  TScreenEffect.Create(FreeAtStop).Inject(Self);
  repeat
    NewTrack := 'mazurka' + (1 + Rnd.Random(5)).ToString;
  until LastTrack <> NewTrack;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName(NewTrack, true);
  LastTrack := NewTrack;
  SoundEngine.LoopingChannel[1].Sound := SoundEngine.SoundFromName('birds' + (1 + Rnd.Random(4)).ToString, true);
  LastNya := '';
  ClickedLayer := -1;

  MapImage := DesignedComponent('MapImage') as TCastleImageControl;
  MapImage.OnPress := @ImagePress;
  MapImage.OnRelease := @ImageRelease; {$WARNING MapImage doesn't receive OnRelease event}

  ScrollViewGameplayArea := DesignedComponent('ScrollViewGameplayArea') as TCastleScrollView;
  ScrollViewGameplayArea.Scroll := 0;

  LabelGirlsRemaining := DesignedComponent('LabelGirlsRemaining') as TCastleLabel;
  LabelCurrentTime := DesignedComponent('LabelCurrentTime') as TCastleLabel;
  LabelHighScore := DesignedComponent('LabelHighScore') as TCastleUserInterface;
  LabelHighScore.Exists := false;

  (DesignedComponent('ContainerGirlsRemaining') as TCastleUserInterface).Exists := true;
  (DesignedComponent('ButtonMenu') as TCastleButton).OnClick := @ClickMenu;
  (DesignedComponent('ButtonMenu') as TCastleUserInterface).Exists := true;
  (DesignedComponent('ButtonWin') as TCastleButton).OnClick := @ClickWin;
  (DesignedComponent('ButtonWin') as TCastleUserInterface).Exists := false;

  StarFall := TStarFallEffect.Create(Self);
  StarFall.FullSize := true;
  StarFall.Duration := 2.0;
  StarFall.Number := 300;
  StarFall.URL := 'castle-data:/ui/particles/particle.png';
  InsertFront(StarFall);

  (DesignedComponent('LabelBestTime') as TCastleLabel).Caption := 'Best: ' + TimeToString(HighScore);

  RandomizeImage;
  BuildImage;
end;

procedure TViewMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  if not GameWon then
  begin
    TimeElapsed += SecondsPassed;
    LabelCurrentTime.Caption := TimeToString(TimeElapsed);
  end;
end;

procedure TViewMain.GLContextOpen;
begin
  inherited GLContextOpen;
  BuildImage;
end;

procedure TViewMain.Resize;
begin
  inherited Resize;
  MapImage.Width := Container.UnscaledWidth;
end;

end.
