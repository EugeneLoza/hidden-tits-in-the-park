{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameInitialize;

interface

procedure ApplySettings;
implementation

uses SysUtils,
  CastleApplicationProperties, CastleWindow, CastleLog, CastleUIControls,
  CastleConfig, CastleSoundEngine, CastleColors,
  SplashScreen
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameViewMain
  , GameViewContentWarning
  , GameViewMainMenu
  , GameViewCredits
  , GameViewOptions
  , GameViewCutscene1
  , GameViewCutscene2
  {$endregion 'Castle Initialization Uses'};

var
  Window: TCastleWindow;

procedure ApplySettings;
begin
  Window.FullScreen := UserConfig.GetValue('fullscreen', true);
  SoundEngine.Volume := UserConfig.GetFloat('master_volume', 0.5);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music_volume', 1.0);
  SoundEngine.LoopingChannel[1].Volume := UserConfig.GetFloat('birds_volume', 0.5);
end;

procedure ApplicationInitialize;
begin
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');
  UserConfig.Load;
  SoundEngine.RepositoryURL := 'castle-data:/audio/index.xml';
  ApplicationProperties.LimitFPS := 60;
  ApplySettings;

  {$region 'Castle View Creation'}
  // The content here may be automatically updated by CGE editor.
  ViewMain := TViewMain.Create(Application);
  ViewContentWarning := TViewContentWarning.Create(Application);
  ViewMainMenu := TViewMainMenu.Create(Application);
  ViewCredits := TViewCredits.Create(Application);
  ViewOptions := TViewOptions.Create(Application);
  ViewCutscene1 := TViewCutscene1.Create(Application);
  ViewCutscene2 := TViewCutscene2.Create(Application);
  {$endregion 'Castle View Creation'}

  { Set default view (the first screen shown to the Player) }
  if UserConfig.GetValue('ask_content_warning', true) or UserConfig.GetValue('always_ask_content_warning', false) then
    Window.Container.View := ViewContentWarning
  else
    Window.Container.View := ViewMainMenu;
end;

initialization
  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2024-2024 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  Application.OnInitialize := @ApplicationInitialize;

  Window := TCastleWindow.Create(Application);
  Application.MainWindow := Window;

  Theme.LoadingBackgroundColor := HexToColor('d5af8e');
  Theme.ImagesPersistent[tiLoading].Image := SplashScreenImage;
  Theme.ImagesPersistent[tiLoading].OwnsImage := false;
  Theme.ImagesPersistent[tiLoading].SmoothScaling := true;
  Theme.LoadingUIScaling := usEncloseReferenceSize;
  Theme.LoadingUIReferenceWidth := 1920;
  Theme.LoadingUIReferenceHeight := 1080;

  {$IFNDEF Android}
    Window.Height := Application.ScreenHeight * 4 div 5;
    Window.Width := Window.Height * 1334 div 750;
  {$ENDIF}

  Window.ParseParameters;
  Window.Container.BackgroundEnable := false;
end.
