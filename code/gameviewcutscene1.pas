{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameViewCutscene1;

interface

uses Classes,
  CastleVectors, CastleUIControls, CastleControls, CastleTimeUtils;

type
  TViewCutscene1 = class(TCastleView)
  strict private
    FirstFrame: Boolean;
    TimeElapsed: Single;
    LoadingText: String;
    procedure ClickContinue(Sender: TObject);
  published
    LabelProgress: TCastleLabel;
    ButtonContinue: TCastleButton;
    DialogueBox1, DialogueBox2: TCastleUserInterface;
    DialogueText1, DialogueText2: TCastleLabel;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
  end;

var
  ViewCutscene1: TViewCutscene1;

implementation
uses
  SysUtils,
  GameViewMain, GameRandom;

procedure TViewCutscene1.ClickContinue(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.View := ViewMain;
end;

constructor TViewCutscene1.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewcutscene1.castle-user-interface';
  DesignPreload := true;
end;

procedure TViewCutscene1.Start;
begin
  inherited;
  FirstFrame := true;
  TimeElapsed := 0;

  LabelProgress.Exists := ViewMain.LoadingNeeded;
  ButtonContinue.Exists := not ViewMain.LoadingNeeded;
  ButtonContinue.OnClick := @ClickContinue;

  DialogueBox1.Exists := false;
  DialogueBox2.Exists := false;

  case Rnd.Random(6) of
    0: LoadingText := 'Randomizing catgirls...';
    1: LoadingText := 'Belly rubs incoming...';
    2: LoadingText := 'Herding cats...';
    3: LoadingText := 'Trimming wiskers...';
    4: LoadingText := 'Opening tuna can...';
    5: LoadingText := 'Uncensoring...';
    else
      LoadingText := 'Loading...';
  end;
  LabelProgress.Caption := LoadingText + ' 0%';
end;

procedure TViewCutscene1.Update(const SecondsPassed: Single;
  var HandleInput: Boolean);
const
  TypingSpeed = 40;
  TypingPause = 10;
var
  Progress: Single;
  FrameTimer: TTimerResult;
  DisplayCharacters: Integer;
begin
  FrameTimer := Timer; // we ignore rendering time here
  inherited;

  DisplayCharacters := Round(TimeElapsed * TypingSpeed);
  TimeElapsed += SecondsPassed;

  DialogueText1.MaxDisplayChars := DisplayCharacters;
  DisplayCharacters -= DialogueText1.Caption.Length + TypingPause;
  DialogueText2.MaxDisplayChars := DisplayCharacters;
  DialogueBox1.Exists := DialogueText1.MaxDisplayChars > 0;
  DialogueBox2.Exists := DialogueText2.MaxDisplayChars > 0;

  if ViewMain.LoadingNeeded then
  begin
    if FirstFrame then
      ViewMain.StartLoadLayers;
    repeat
      Progress := ViewMain.LoadNextFile;
      if Progress >= 1.0 then
        ViewMain.FinishLoadLayers;
    until (FrameTimer.ElapsedTime >= 0.03) or not ViewMain.LoadingNeeded;
    LabelProgress.Caption := LoadingText + ' ' + Trunc(100 * Progress).ToString + '%';
    LabelProgress.Exists := ViewMain.LoadingNeeded;
    ButtonContinue.Exists := not ViewMain.LoadingNeeded;
  end;
  FirstFrame := false;
end;

end.
