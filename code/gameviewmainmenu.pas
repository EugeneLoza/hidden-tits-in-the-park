{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameViewMainMenu;

interface

uses Classes,
  CastleVectors, CastleUIControls, CastleControls;

type
  TViewMainMenu = class(TCastleView)
  private
    procedure ClickCredits(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickPlay(Sender: TObject);
    procedure ClickQuit(Sender: TObject);
  published
    { Components designed using CGE editor.
      These fields will be automatically initialized at Start. }
    // ButtonXxx: TCastleButton;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
  end;

var
  ViewMainMenu: TViewMainMenu;

implementation
uses
  SysUtils,
  CastleWindow, CastleApplicationProperties, CastleSoundEngine,
  GameViewCredits, GameViewMain, GameViewOptions, GameViewCutscene1, GameScreenEffect,
  GameRandom;

procedure TViewMainMenu.ClickCredits(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.PushView(ViewCredits);
end;

procedure TViewMainMenu.ClickOptions(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.PushView(ViewOptions);
end;

procedure TViewMainMenu.ClickPlay(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.View := ViewCutscene1;
end;

procedure TViewMainMenu.ClickQuit(Sender: TObject);
begin
  TViewMain.PopSound;
  Application.MainWindow.Close;
end;

constructor TViewMainMenu.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewmainmenu.castle-user-interface';
  DesignPreload := true;
end;

procedure TViewMainMenu.Start;
begin
  inherited;
  TScreenEffect.Create(FreeAtStop).Inject(Self);
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('tango', true);
  SoundEngine.LoopingChannel[1].Sound := SoundEngine.SoundFromName('birds' + (1 + Rnd.Random(4)).ToString, true);

  (DesignedComponent('ButtonQuit') as TCastleButton).OnClick := @ClickQuit;
  (DesignedComponent('ButtonQuit') as TCastleButton).Exists := not ApplicationProperties.TouchDevice;
  (DesignedComponent('ButtonStart') as TCastleButton).OnClick := @ClickPlay;
  (DesignedComponent('ButtonOptions') as TCastleButton).OnClick := @ClickOptions;
  (DesignedComponent('ButtonCredits') as TCastleButton).OnClick := @ClickCredits;
end;

procedure TViewMainMenu.Update(const SecondsPassed: Single; var HandleInput: boolean);
begin
  inherited;
  { Executed every frame. }
end;

end.
