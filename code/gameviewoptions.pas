{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameViewOptions;

interface

uses Classes,
  CastleVectors, CastleUIControls, CastleControls;

type
  TViewOptions = class(TCastleView)
  private
    procedure BirdsChanged(Sender: TObject);
    procedure ClickFullScreen(Sender: TObject);
    procedure MusicChanged(Sender: TObject);
    procedure ReturnToMainMenu(Sender: TObject);
    procedure VolumeChanged(Sender: TObject);
  published
    { Components designed using CGE editor.
      These fields will be automatically initialized at Start. }
    // ButtonXxx: TCastleButton;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Stop; override;
  end;

var
  ViewOptions: TViewOptions;

implementation
uses
  CastleConfig, CastleApplicationProperties,
  GameViewMain, GameScreenEffect, GameInitialize;

function OnOff(const Value: Boolean): String;
begin
  if Value then
    Exit('ON')
  else
    Exit('OFF');
end;

procedure TViewOptions.ClickFullScreen(Sender: TObject);
begin
  TViewMain.PopSound;
  UserConfig.SetValue('fullscreen', not UserConfig.GetValue('fullscreen', true));
  ApplySettings;
  (DesignedComponent('ButtonFullScreen') as TCastleButton).Caption := OnOff(UserConfig.GetValue('fullscreen', false));
end;

procedure TViewOptions.ReturnToMainMenu(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.PopView(Self);
end;

procedure TViewOptions.VolumeChanged(Sender: TObject);
begin
  UserConfig.SetFloat('master_volume', (DesignedComponent('SliderVolume') as TCastleFloatSlider).Value);
  ApplySettings;
end;

procedure TViewOptions.MusicChanged(Sender: TObject);
begin
  UserConfig.SetFloat('music_volume', (DesignedComponent('SliderMusic') as TCastleFloatSlider).Value);
  ApplySettings;
end;

procedure TViewOptions.BirdsChanged(Sender: TObject);
begin
  UserConfig.SetFloat('birds_volume', (DesignedComponent('SliderBirds') as TCastleFloatSlider).Value);
  ApplySettings;
end;

constructor TViewOptions.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewoptions.castle-user-interface';
  DesignPreload := true;
  InterceptInput := true;
end;

procedure TViewOptions.Start;
begin
  inherited;
  TScreenEffect.Create(FreeAtStop).Inject(Self);
  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ReturnToMainMenu;
  (DesignedComponent('ButtonFullScreen') as TCastleButton).OnClick := @ClickFullScreen;
  (DesignedComponent('GroupFullScreen') as TCastleUserInterface).Exists := not ApplicationProperties.TouchDevice;

  (DesignedComponent('SliderVolume') as TCastleFloatSlider).OnChange := @VolumeChanged;
  (DesignedComponent('SliderMusic') as TCastleFloatSlider).OnChange := @MusicChanged;
  (DesignedComponent('SliderBirds') as TCastleFloatSlider).OnChange := @BirdsChanged;

  (DesignedComponent('SliderVolume') as TCastleFloatSlider).Value := UserConfig.GetFloat('master_volume', 0.5);
  (DesignedComponent('SliderMusic') as TCastleFloatSlider).Value := UserConfig.GetFloat('music_volume', 1.0);
  (DesignedComponent('SliderBirds') as TCastleFloatSlider).Value := UserConfig.GetFloat('birds_volume', 0.5);
  (DesignedComponent('ButtonFullScreen') as TCastleButton).Caption := OnOff(UserConfig.GetValue('fullscreen', true));
end;

procedure TViewOptions.Stop;
begin
  inherited Stop;
  UserConfig.Save;
end;


end.
