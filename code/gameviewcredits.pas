{ Copyright (C) 2024-2024 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ Initialize game window, load data and set up UI (views)
  everything starts here }
unit GameViewCredits;

interface

uses Classes,
  CastleVectors, CastleUIControls, CastleControls;

type
  TViewCredits = class(TCastleView)
  private
    procedure ClickOnlineCredits(Sender: TObject);
    procedure ReturnToMainMenu(Sender: TObject);
  published
    { Components designed using CGE editor.
      These fields will be automatically initialized at Start. }
    // ButtonXxx: TCastleButton;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
  end;

var
  ViewCredits: TViewCredits;

implementation
uses
  CastleOpenDocument,
  GameViewMain, GameScreenEffect;

procedure TViewCredits.ClickOnlineCredits(Sender: TObject);
begin
  TViewMain.PopSound;
  OpenUrl('https://gitlab.com/EugeneLoza/hidden-tits-in-the-park/-/blob/grandmaster/CREDITS.md');
end;

procedure TViewCredits.ReturnToMainMenu(Sender: TObject);
begin
  TViewMain.PopSound;
  Container.PopView(Self);
end;

constructor TViewCredits.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/ui/gameviewcredits.castle-user-interface';
  DesignPreload := false;
  InterceptInput := true;
end;

procedure TViewCredits.Start;
begin
  inherited;
  TScreenEffect.Create(FreeAtStop).Inject(Self);
  (DesignedComponent('ButtonReturn') as TCastleButton).OnClick := @ReturnToMainMenu;
  (DesignedComponent('ButtonOnlineCredits') as TCastleButton).OnClick := @ClickOnlineCredits;
end;

procedure TViewCredits.Update(const SecondsPassed: Single; var HandleInput: boolean);
begin
  inherited;
  { Executed every frame. }
end;

end.
