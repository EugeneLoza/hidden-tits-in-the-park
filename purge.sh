#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

find -type d -name 'backup' -prune -exec rm -rf {} \;
rm -f *.dbg
rm -f hidden_tits_in_the_park
rm -f hidden_tits_in_the_park.exe
rm -rf castle-engine-output/standalone
rm -f *.res
#rm -rf dockerbuild ---- doesn't do so yet
