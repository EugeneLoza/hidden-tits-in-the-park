# 1.0.2 - Quality of Life
* Darker outline for particles
* Count time spent and show notification "Best Time!" when hitting a high score
* Cutscenes text now doesn't advance when app is in background
* Colorize dialogue boxes for tits
* Fix credits according to Creative Commons recommendation
* Fix loading progress label blinking wrong text for one frame
* Fix non-nicely looking background on splash screen

# 1.0.1 - hotfix for Android
A quick hotfix for map disappearing when switching apps on Android.

#1.0.0 - initial release
