# Third party assets used in the game

TALS-style (title-author-license-source-changes) credits recommended by Creative Commons https://wiki.creativecommons.org/wiki/Recommended_practices_for_attribution

## References to licenses details

* CC-BY 3.0 - https://creativecommons.org/licenses/by/3.0/
* CC-BY 4.0 - https://creativecommons.org/licenses/by/4.0/
* CC-0 - https://creativecommons.org/publicdomain/zero/1.0/
* SIL Open Font License (OFL) https://openfontlicense.org/

#### Fonts

/data/ui/fonts/emilys-candy/EmilysCandy-Regular.ttf
https://www.1001fonts.com/emilys-candy-font.html
Copyright (c) 2012, Font Diner (www.fontdiner.com),
with Reserved Font Name "Emilys Candy".
licensed under the SIL Open Font License, Version 1.1.

/data/ui/fonts/kalam/Kalam-Bold.ttf
/data/ui/fonts/kalam/Kalam-Regular.ttf
OFL by Lipi Raval, Jonny Pinhorn
https://www.1001fonts.com/kalam-font.html

#### Graphical assets

/data/ui/cge/cge_logo.png
Copyright 2008-2018 Paweł Wojciechowicz, Michalis Kamburelis, Katarzyna Obrycka.
Distributed on the same license as the Castle Game Engine itself: GNU GPL >= 2 or GNU LGPL >= 2, with static linking exception; Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
https://github.com/castle-engine/castle-engine/tree/master/doc/pasdoc/logo

/data/ui/background/hard-paper-texture_CC0_by_Petr_Kratochvil.jpg
CC0 by Petr Kratochvil
https://www.publicdomainpictures.net/en/view-image.php?image=26028

/data/ui/tits/Japanese-Tit-on-a-twig.png
CC0 by yamachem
https://openclipart.org/detail/250672/japanese-tit-on-a-twig
see https://openclipart.org/share for license and use information

/data/ui/tits/Varied-tit-01-remix.png
CC0 by yamachem
https://openclipart.org/detail/259790/varied-tit-01-remix
see https://openclipart.org/share for license and use information

#### Music

/data/audio/music/mazurka1_CC-BY_by_Bruno_Belotti.ogg
CC-BY-SA 3.0 by Bruno Belotti
https://www.jamendo.com/artist/348519/bruno-belotti
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/music/mazurka2_CC-BY_by_Bruno_Belotti.ogg
CC-BY-SA 3.0 by Bruno Belotti
https://www.jamendo.com/artist/348519/bruno-belotti
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/music/mazurka3_CC-BY_by_Bruno_Belotti.ogg
CC-BY-SA 3.0 by Bruno Belotti
https://www.jamendo.com/artist/348519/bruno-belotti
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/music/mazurka4_CC-BY_by_Bruno_Belotti.ogg
CC-BY-SA 3.0 by Bruno Belotti
https://www.jamendo.com/artist/348519/bruno-belotti
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/music/mazurka5_CC-BY_by_Bruno_Belotti.ogg
CC-BY-SA 3.0 by Bruno Belotti
https://www.jamendo.com/artist/348519/bruno-belotti
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/music/tango_CC-BY_by_Bruno_Belotti.ogg
CC-BY-SA 3.0 by Bruno Belotti
https://www.jamendo.com/artist/348519/bruno-belotti
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

#### Sound assets

/data/audio/birds/569914__titi2__forest_after_rain_210429_0075_CC0_by_titi2[1].ogg
CC0 by titi2
https://freesound.org/people/titi2/sounds/569914/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/birds/569914__titi2__forest_after_rain_210429_0075_CC0_by_titi2[2].ogg
CC0 by titi2
https://freesound.org/people/titi2/sounds/569914/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/birds/569915__titi2__after_rain_210429_0074_CC0_by_titi2[1].ogg
CC0 by titi2
https://freesound.org/people/titi2/sounds/569915/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/birds/569915__titi2__after_rain_210429_0074_CC0_by_titi2[2].ogg
CC0 by titi2
https://freesound.org/people/titi2/sounds/569915/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/238641__madamvicious__anime-magical-girl-voice_CC0_by_MadamVicious[1].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/238641/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/238641__madamvicious__anime-magical-girl-voice_CC0_by_MadamVicious[2].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/238641/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/238641__madamvicious__anime-magical-girl-voice_CC0_by_MadamVicious[4].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/238641/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/238641__madamvicious__anime-magical-girl-voice_CC0_by_MadamVicious[5].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/238641/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/238641__madamvicious__anime-magical-girl-voice_CC0_by_MadamVicious[6].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/238641/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/238641__madamvicious__anime-magical-girl-voice_CC0_by_MadamVicious[7].wav
CC0 by MadamVicious
https://freesound.org/people/MadamVicious/sounds/238641/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[1].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[2].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[3].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[4].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[5].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[6].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[7].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/catgirls/476588__kurireeva__anime-cat-girl_CC0_by_KurireeVA[8].wav
CC0 by KurireeVA
https://freesound.org/people/KurireeVA/sounds/476588/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/cheering/429422__zeraora__audience-clapping_CC0_by_Zeraora[FadeOut].wav
CC0 by Zeraora
https://freesound.org/people/Zeraora/sounds/429422/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[1].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[2].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[3].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[4].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[5].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[6].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[7].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[8].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0

/data/audio/sounds/pops/75353__neotone__popping-sample-pack_CC0_by_Neotone[9].wav
CC0 by Neotone
https://freesound.org/people/Neotone/sounds/75353/
Trimmed, cropped, normalized by EugeneLoza
Changes are licensed under CC0
